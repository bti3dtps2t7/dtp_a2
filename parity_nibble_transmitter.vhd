
library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;



entity parity_nibble_transmitter is
port(
    rxdat : out std_logic_vector( 3 downto 0 ); -- empfangene Daten
    ok : out std_logic; -- Empfangsstatus
    txdat : in std_logic_vector( 3 downto 0 ); -- zu übertragende Daten
    clk : in std_logic; -- Takt
    nres : in std_logic -- synchronous RESET, low active
    );
end entity parity_nibble_transmitter;


architecture beh_parity_nibble_transmitter of parity_nibble_transmitter is

signal txdat_s : std_logic_vector(3 downto 0);
signal clk_s : std_logic;
signal nres_s : std_logic;
signal sender_out_s : std_logic_vector(4 downto 0);
signal empfaenger_out_s : std_logic_vector(3 downto 0);
signal ok_s : std_logic;


component sender is
    port(
        rxdat : out std_logic_vector( 4 downto 0 ); -- empfangene Daten
        txdat : in std_logic_vector( 3 downto 0 );  -- zu übertragende Daten
        clk : in std_logic;                         -- Takt
        nres : in std_logic                  -- synchronous RESET, low active
        );
end component sender;
for all : sender use entity work.sender( beh_sender );

component empfaenger is
    port(
        txdat : in std_logic_vector(4 downto 0);
        nres : in std_logic;
        clk : in std_logic;
        rxdat : out std_logic_vector(3 downto 0);
        ok : out std_logic
        );
end component empfaenger;
for all : empfaenger use entity work.empfaenger( beh_empf );

begin

txdat_s <= txdat;
clk_s <= clk;
nres_s <= nres;

sender_i : sender
port map(
        txdat => txdat_s,
        clk => clk_s,
        nres => nres_s,
        rxdat => sender_out_s
);

empfaenger_i : empfaenger
port map(
        txdat => sender_out_s,
        nres => nres_s,
        clk => clk_s,
        rxdat => empfaenger_out_s,
        ok => ok_s
    );

rxdat <= empfaenger_out_s;
ok <= ok_s;


end architecture beh_parity_nibble_transmitter;


