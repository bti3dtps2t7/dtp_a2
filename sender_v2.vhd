-- Der Sender besteht aus einem 5-bit-Eingangsregister und einem 5-bit Ausgangsregister.
-- Vom Eingangsregister werden nur 4 Bit verwendet.
-- Dazwischen sitzt der Parity Generator


library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;




entity sender is
	port(
        rxdat : out std_logic_vector( 4 downto 0 ); -- empfangene Daten
        txdat : in std_logic_vector( 3 downto 0 );  -- zu übertragende Daten
        clk : in std_logic;                         -- Takt
        nres : in std_logic                  -- synchronous RESET, low active
		);

end entity sender;

architecture beh_sender of sender is
    signal input_cs : std_logic_vector(3 downto 0):= (others => '0');
    signal input_ns : std_logic_vector(3 downto 0):= (others => '0');
    signal output_cs : std_logic_vector(4 downto 0):= (others => '0');
    signal output_ns : std_logic_vector(4 downto 0):= (others => '0');
    signal clk_s : std_logic := '0';
    signal nres_s : std_logic:= '0';


    begin

    input_ns <= txdat;

    reg:
    process (clk)
    begin
        if clk = '1' and clk'event  then
            if nres = '0'   then
                input_cs <= (others=>'0');
                output_cs <= (others=>'0');
            else
                input_cs <= input_ns;
                output_cs <= output_ns;
            end if;
        end if;
    end process reg;



    paritygen:
    process (input_cs)
    variable parity_bit_v : std_logic;
    variable data_in_v : std_logic_vector(3 downto 0);
    begin

    data_in_v := input_cs;

    parity_bit_v := ((data_in_v(3) XOR data_in_v(2)) XNOR (data_in_v(1) XOR data_in_v(0)));


    -- Das Parity Bit wird an die Stelle 4 (=MSB) des Busses gepackt.
    
    output_ns <= parity_bit_v & data_in_v;


    end process paritygen; 

    rxdat <= output_cs;

end architecture beh_sender;
