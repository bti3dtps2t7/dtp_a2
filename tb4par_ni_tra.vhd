library work;
    use work.all;                                           -- Zugriff auf Arbeitsbibliothek ("dorthin", wohin alles compiliert wird)

library ieee;
    use ieee.std_logic_1164.all;                            -- benoetigt fuer std_logic


entity tb4par_ni_tra is
end entity tb4par_ni_tra;


architecture beh_tb of tb4par_ni_tra is

signal nres_s : std_logic;
signal clk_s : std_logic;
signal ok_s : std_logic;
signal data_s : std_logic_vector(3 downto 0);
signal data_out_s : std_logic_vector(3 downto 0);

component parity_nibble_transmitter is
port(
	rxdat : out std_logic_vector( 3 downto 0 ); -- empfangene Daten
    ok : out std_logic; -- Empfangsstatus
    txdat : in std_logic_vector( 3 downto 0 ); -- zu übertragende Daten
    clk : in std_logic; -- Takt
    nres : in std_logic -- synchronous RESET, low active
	);
end component parity_nibble_transmitter;
for all : parity_nibble_transmitter use entity work.parity_nibble_transmitter( beh_parity_nibble_transmitter );

component sg is
port(
	data : out std_logic_vector( 3 downto 0);
    clk  : out std_logic;
    nres : out std_logic
	);
end component sg;
for all : sg use entity work.sg( beh );


begin


sg_i : sg
port map(
		data => data_s,
		clk => clk_s,
		nres => nres_s
		);


parity_nibble_transmitter_i : parity_nibble_transmitter
port map(
		txdat => data_s,
		clk => clk_s,
		nres => nres_s,
		rxdat => data_out_s,
		ok => ok_s
		);

end architecture beh_tb;


architecture beh_tb_time of tb4par_ni_tra is

signal nres_s : std_logic;
signal clk_s : std_logic;
signal ok_s : std_logic;
signal data_s : std_logic_vector(3 downto 0);
signal data_out_s : std_logic_vector(3 downto 0);

component parity_nibble_transmitter_time is
port(
	rxdat : out std_logic_vector( 3 downto 0 ); -- empfangene Daten
    ok : out std_logic; -- Empfangsstatus
    txdat : in std_logic_vector( 3 downto 0 ); -- zu übertragende Daten
    clk : in std_logic; -- Takt
    nres : in std_logic -- synchronous RESET, low active
	);
end component parity_nibble_transmitter_time;
for all : parity_nibble_transmitter_time use entity work.parity_nibble_transmitter_time( Structure );

component sg is
port(
	data : out std_logic_vector( 3 downto 0);
    clk  : out std_logic;
    nres : out std_logic
	);
end component sg;
for all : sg use entity work.sg( beh );


begin


sg_i : sg
port map(
		data => data_s,
		clk => clk_s,
		nres => nres_s
		);


parity_nibble_transmitter_i : parity_nibble_transmitter_time
port map(
		txdat => data_s,
		clk => clk_s,
		nres => nres_s,
		rxdat => data_out_s,
		ok => ok_s
		);

end architecture beh_tb_time;