-- Der Empfänger besteht aus einem 5-bit-Eingangsregister und einem 4+1 bit Ausgangsregister.
-- Register sind als Prozess implementiert.
-- Dazwischen sitzt der Parity Checker


library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;


entity empfaenger is
    port(
        txdat : in std_logic_vector(4 downto 0);
        nres : in std_logic;
        clk : in std_logic;
        rxdat : out std_logic_vector(3 downto 0);
        ok : out std_logic
        );
end entity empfaenger;

architecture beh_empf of empfaenger is
    signal ok_ns : std_logic := '0';
    signal ok_cs : std_logic := '0';
    signal txdat_s : std_logic_vector(4 downto 0) := (others => '0');
    signal input_cs : std_logic_vector(4 downto 0):= (others => '0');
    signal input_ns : std_logic_vector(4 downto 0):= (others => '0');
    signal output_cs : std_logic_vector(3 downto 0):= (others => '0');
    signal output_ns : std_logic_vector(3 downto 0):= (others => '0');


begin
    
    input_ns <= txdat;

    reg:
    process (clk) is
    begin
        if clk = '1' and clk'event  then
            if nres = '0'   then
                input_cs <= (others=>'0');
                output_cs <= (others=>'0');
                ok_cs <= '0';
            else
                input_cs <= input_ns;
                output_cs <= output_ns;
                ok_cs <= ok_ns;
            end if;
        end if;
    end process reg; 



    paritycheck:
    process (input_cs) is
        variable data_in_v : std_logic_vector(3 downto 0);
        variable parity_bit_v : std_logic;
    begin

        data_in_v := input_cs(3 downto 0);

        parity_bit_v := ((data_in_v(3) XOR data_in_v(2)) XNOR (data_in_v(1) XOR data_in_v(0)));

        if(parity_bit_v = input_cs(4)) then
            ok_ns <= '1';
        else
            ok_ns <= '0';
        end if;

    end process paritycheck;

    ok <= ok_cs;
    output_ns <= input_cs(3 downto 0);
    rxdat <= output_cs;

end architecture beh_empf;