vsim -sdftyp /parity_nibble_transmitter_i=D:/DTP/dtp_a2/ISE_14x4_v2/ise_work/netgen/fit/parity_nibble_transmitter_timesim.sdf -t 10ps -novopt work.tb4par_ni_tra(beh_tb_time)


add wave /tb4par_ni_tra/parity_nibble_transmitter_i/txdat  
add wave /tb4par_ni_tra/parity_nibble_transmitter_i/sender_i_input_cs_0_Q/O
add wave /tb4par_ni_tra/parity_nibble_transmitter_i/sender_i_input_cs_1_Q/O 
add wave /tb4par_ni_tra/parity_nibble_transmitter_i/sender_i_input_cs_2_Q/O 
add wave /tb4par_ni_tra/parity_nibble_transmitter_i/sender_i_input_cs_3_Q/O  
add wave /tb4par_ni_tra/parity_nibble_transmitter_i/clk    

run -all

wave zoom full